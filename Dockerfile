#FROM node:lts-alpine AS build

#WORKDIR /app

#COPY package.json package-lock.json ./

#RUN npm i

#COPY . .

#ENV NODE_ENV production
#RUN npm run build

FROM nginx

COPY ./build /app/frontend
