const path = require('path');
const webpackNodeExternals = require('webpack-node-externals');

module.exports = {
  mode: 'production',
  target: 'node',
  entry: './server/index.js',
  output: {
    path: path.resolve(__dirname),
    filename: 'server.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        use: ['isomorphic-style-loader', { loader: 'css-loader' }]
      }
    ]
  },
  externals: [webpackNodeExternals()],
  resolve: {
    alias: {
      reducers: path.resolve(__dirname, './src/redux/reducers'),
      containers: path.resolve(__dirname, './src/containers/'),
      constants: path.resolve(__dirname, './src/constants/'),
      config: path.resolve(__dirname, './src/config/'),
      uiKit: path.resolve(__dirname, './src/components/ui-kit')
    }
  }
};
