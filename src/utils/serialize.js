export default (obj) => {
  const str = [];
  Object.keys(obj).map(key => str.push(`${encodeURIComponent(key)}=${encodeURIComponent(obj[key])}`));

  return str.join('&');
};
