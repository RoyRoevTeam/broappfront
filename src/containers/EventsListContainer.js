import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { fetchEventsAction } from 'reducers/events';

const mapStateToProps = ({ events: { list, isLoading } }) => ({
  list,
  isLoading
});

const mapDispatchToProps = {
  fetchEvents: fetchEventsAction
};

const propTypes = {
  fetchEvents: PropTypes.func.isRequired
};

const eventsStub = [
  {
    _id: 1,
    imgUrl: 'https://picsum.photos/200/300/?random',
    title: '1',
    date: '10-10-2019'
  },
  {
    _id: 2,
    imgUrl: 'https://picsum.photos/200/300/?random',
    title: '2',
    date: '10-10-2019'
  }
];

export default Target => {
  class EventsListContainer extends PureComponent {
    static propTypes = propTypes;

    componentDidMount() {
      const { fetchEvents } = this.props;

      fetchEvents();
    }

    render() {
      return <Target {...this.props} list={this.props.list.length ? this.props.list : eventsStub} />;
    }
  }

  return connect(mapStateToProps, mapDispatchToProps)(EventsListContainer);
};
