import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { formValueSelector } from 'redux-form';

import eventCategories from '../data/event-categories';

const mapStateToProps = state => {
  const eventCreationSelector = formValueSelector('eventCreation');

  return {
    selectedCategory: eventCreationSelector(state, 'category')
  };
};

export default Target => {
  class EventCategoriesContainer extends PureComponent {
    render() {
      return <Target eventCategories={eventCategories} {...this.props} />;
    }
  }

  return connect(mapStateToProps)(EventCategoriesContainer);
};
