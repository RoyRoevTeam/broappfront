import React from 'react';
import PropTypes from 'prop-types';
import Navigate from 'components/Navigate';

import styles from './BaseLayout.module.scss';

const BaseLayout = ({ children }) => (
  <div className={styles.wrapper}>
    <header className={styles}>
      <Navigate />
    </header>
    <div className={styles.content}>
      {children}
    </div>
    <footer className={styles.footer}>
      <span className={styles}>BROapp 2019 ©</span>
    </footer>
  </div>
);

BaseLayout.propTypes = {
  children: PropTypes.node.isRequired
};

export default BaseLayout;
