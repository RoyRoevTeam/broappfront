import React from 'react';
import PropTypes from 'prop-types';

import styles from './Section.css';

const Section = props => {
  const { children } = props;

  return (
    <section className={styles.wrapper}>
      {children}
    </section>
  );
};

Section.propTypes = {
  children: PropTypes.node.isRequired
};

export default Section;
