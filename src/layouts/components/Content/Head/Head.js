import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './Head.css';
import { Icons } from '../../../../components/ui-kit';

const Head = props => {
  const {
    children, title, description, fullHeight, onGoBack, onClose
  } = props;

  const wrapClasses = classNames(
    styles.headWrap,
    { [styles['headWrap--fullHeight']]: fullHeight === true }
  );

  return (
    <section className={wrapClasses}>
      <div className={styles.head}>
        <div className={styles.controlsWrap}>
          {typeof onGoBack === 'function' &&
            <span className={styles.goBackWrap} onClick={onGoBack}>
              <Icons.Arrow />
            </span>
          }
          {typeof onClose === 'function' &&
            <span className={styles.closeWrap} onClick={onClose}>
              <Icons.Close />
            </span>
          }
        </div>
        {title && <h1 className={styles.title}>{title}</h1>}
        {description && <p className={styles.description}>{description}</p>}
        <div>{children}</div>
      </div>
    </section>
  );
};

Head.propTypes = {
  children: PropTypes.node,
  title: PropTypes.string,
  description: PropTypes.string,
  fullHeight: PropTypes.bool,
  onGoBack: PropTypes.func,
  onClose: PropTypes.func
};

Head.defaultProps = {
  children: null,
  title: null,
  description: null,
  fullHeight: false,
  onGoBack: null,
  onClose: null
};

export default Head;
