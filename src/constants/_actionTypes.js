export const SET_STEP = 'registration/set-step';

/* user */
export const SETUP_USER = 'registration/setup-user';
export const AUTH_USER = 'registration/auth-user';

/* events */
export const FETCH_EVENTS = 'events/fetch';
export const FETCH_EVENTS_SUCCESS = 'events/fetch-success';
export const FETCH_EVENTS_FAILED = 'events/fetch-failed';

export const SAVE_EVENTS = 'events/save';
export const SAVE_EVENTS_SUCCESS = 'events/save-success';
export const SAVE_EVENTS_FAILED = 'events/save-failed';

/* event creation */
export const CHOOSE_EVENT_CATEGORY = 'event-creation/choose-event-category';
export const SET_ANOTHER_CATEGORY = 'event-creation/set-another-category';
