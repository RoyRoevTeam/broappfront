import React from 'react';
import io from 'socket.io-client';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { AppContainer, setConfig } from 'react-hot-loader';
import { PersistGate } from 'redux-persist/integration/react';

import './styles/index.css';
import './styles/colors.css';
import App from './components/App';
import createStore, { history } from './redux/store';
import registerServiceWorker from './utils/registerServiceWorker';
import { API_URL } from './config';

setConfig({
  pureRender: true
});

export const socket = io.connect(API_URL);

const { store, persistor } = createStore();

const render = () => {
  const root = document.getElementById('root');

  ReactDOM.render(
    <AppContainer>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <App history={history} />
        </PersistGate>
      </Provider>
    </AppContainer>,
    root
  );
};

if (module.hot) {
  module.hot.accept('./components/App', () => {
    render();
  });
}

render();

registerServiceWorker();
