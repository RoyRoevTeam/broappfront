import Axios from 'axios';
import { push } from 'connected-react-router';
import { SubmissionError } from 'redux-form';
import { API_URL } from 'config';

import * as actionTypes from 'constants/_actionTypes';

export const axios = Axios.create({
  baseURL: `${API_URL}/`,
  timeout: 60000,
  headers: {
    Accept: 'application/json'
  },
  withCredentials: true
});

const initialState = {
  profile: null,
  isLoading: false
};

export const setUserAction = user => ({
  type: actionTypes.AUTH_USER,
  payload: user
});

export const checkAuthUser = () => dispatch => axios.get('/auth/authenticated')
  .then(({ data }) => dispatch(setUserAction(data)))
  .catch(({ response: { data: { message } = {} } = {} } = {}) => {
    console.error(message);
  });

export const authUser = user => dispatch => axios.post('/auth/login', user)
  .then(({ data }) => dispatch(setUserAction(data)))
  .then(() => dispatch(push('/profile')))
  .catch(({ response: { data: { message } = {} } = {} } = {}) => {
    throw new SubmissionError({
      _error: message
    });
  });

const user = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.AUTH_USER:
      return {
        ...state,
        profile: action.payload
      };
    default:
      return state;
  }
};

export default user;
