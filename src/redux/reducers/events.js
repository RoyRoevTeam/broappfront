import Axios from 'axios';
import { API_URL } from 'config';

import * as actionTypes from 'constants/_actionTypes';

export const axios = Axios.create({
  baseURL: `${API_URL}/`,
  timeout: 60000,
  headers: {
    Accept: 'application/json'
  },
  withCredentials: true
});

const initialState = {
  list: [],
  isLoading: false,
  error: null
};

export const fetchEventsBegin = () => ({
  type: actionTypes.FETCH_EVENTS
});

export const fetchEventsSuccess = events => ({
  type: actionTypes.FETCH_EVENTS_SUCCESS,
  payload: events
});

export const fetchEventsFailed = error => ({
  type: actionTypes.FETCH_EVENTS_FAILED,
  payload: error
});

export const saveEventsBegin = () => ({
  type: actionTypes.SAVE_EVENTS
});

export const saveEventsSuccess = events => ({
  type: actionTypes.SAVE_EVENTS_SUCCESS,
  payload: events
});

export const saveEventsFailed = error => ({
  type: actionTypes.SAVE_EVENTS_FAILED,
  payload: error
});

export const fetchEventsAction = () => dispatch => {
  dispatch(fetchEventsBegin());

  axios.get('/events')
    .then(({ data }) => dispatch(fetchEventsSuccess(data)))
    .catch(({ response: { data: { message } = {} } = {} } = {}) => {
      fetchEventsFailed(message);
    });
};

export const createEvent = event => (dispatch, getState) => {
  console.log(event);
  dispatch(saveEventsBegin());

  const userId = getState().user.profile.id;

  axios.post('/events', { ...event, createdBy: userId })
    .then(({ data }) => dispatch(saveEventsSuccess(data)))
    .catch(({ response: { data: { message } = {} } = {} } = {}) => {
      saveEventsFailed(message);
    });
};

const events = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_EVENTS:
      return {
        ...state,
        isLoading: true,
        list: []
      };

    case actionTypes.FETCH_EVENTS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        list: action.payload
      };

    case actionTypes.FETCH_EVENTS_FAILED:
      return {
        ...state,
        list: [],
        isLoading: false,
        error: action.payload
      };

    default:
      return state;
  }
};

export default events;
