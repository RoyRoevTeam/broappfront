import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { reducer as formReducer } from 'redux-form';

import registration from './registration';
import user from './user';
import events from './events';

export default history => combineReducers({
  router: connectRouter(history),
  form: formReducer,
  registration,
  user,
  events
});
