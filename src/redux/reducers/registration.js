import * as actionTypes from '../../constants/_actionTypes';

const initialState = {
  currentStep: 'userInfo'
};

export const setStep = step => ({
  type: actionTypes.SET_STEP,
  step
});

const registration = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_STEP:
      return {
        ...state,
        currentStep: action.step
      };
    default:
      return state;
  }
};

export default registration;
