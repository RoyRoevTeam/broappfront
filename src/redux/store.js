/* global window */
import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'connected-react-router';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import thunk from 'redux-thunk';
import { createBrowserHistory } from 'history';

import createRootReducer from './reducers';

export const history = createBrowserHistory();

export const initializeSession = () => ({
  type: 'INITIALIZE_SESSION'
});

const persistConfig = {
  key: 'root',
  storage,
  blacklist: ['form', 'registration']
};

const initialState = {};
const enhancers = [];
const middleware = [
  thunk,
  routerMiddleware(history)
];

if (process.env.NODE_ENV === 'development') {
  const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__; // eslint-disable-line

  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension());
  }
}

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers,
);

const persistedReducer = persistReducer(persistConfig, createRootReducer(history));

export default function configureStore() {
  const store = createStore(
    persistedReducer,
    initialState,
    composedEnhancers,
  );
  const persistor = persistStore(store);

  return { store, persistor };
};
