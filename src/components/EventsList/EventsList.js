import React from 'react';
import EventPreview from 'components/EventPreview';
import Container from 'containers/EventsListContainer';
import { Content } from 'layouts';
import { Link } from 'uiKit';
import { Search } from 'uiKit/Icons';

import styles from './EventsList.scss';

const EventsList = ({ list, isLoading }) => (
  <div className={styles.eventsListContainer}>
    <div className={styles.searchMenu}>
      <div>
        <button type="button" className={styles.btn}>
          Все события 296
          <i className={styles.rightArrow} />
        </button>
      </div>
      <Search className={styles.searchIcon} />
    </div>
    <div className={styles.eventsList}>
      {list.length ? list.map(({
        _id, imgUrl, title, date
      }) => (
        <EventPreview key={_id} {...{ imgUrl, title, date }} />
      )) : isLoading ? (
        <div>
          <span>Loading...</span>
        </div>
      ) : (
        <div>
          <Content.Row>
            <div>Пока нет созданных событий, будь первым!</div>
          </Content.Row>
          <Content.Row>
            <Link to="/eventCreation" fluid>
              Создать событие
            </Link>
          </Content.Row>
        </div>
      )}
    </div>
  </div>

);

export default Container(EventsList);
