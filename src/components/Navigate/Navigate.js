import React from 'react';
import { Link } from 'react-router-dom';
import useReactRouter from 'utils/use-react-router';
import cn from 'classnames';
import {
  Chat, Events, MyEvents, Plus, User
} from 'uiKit/Icons';
import styles from './Navigate.scss';


const menuItems = [
  { link: '/', title: 'События', icon: Events },
  { link: '/eventCreation', title: 'Мои события', icon: MyEvents },
  { link: '/eventCreation', title: 'Новое событие', icon: Plus },
  { link: '/chat', title: 'Чат', icon: Chat },
  { link: '/profile', title: 'Профиль', icon: User }
  // { link: '/registration', title: 'Зарегистрироваться' },
  // { link: '/authorization', title: 'Войти' },
];

const Navigate = () => {
  const { match } = useReactRouter();

  return (
    <nav className={styles.navBar}>
      <div className={styles.brand}>
        <h1><Link to="/">BROapp</Link></h1>
      </div>
      <ul className={styles.menuItems}>
        {menuItems.map(({ link, title, icon: Icon }, index) => (
          <li key={index} className={styles.menuItem}>
            <Link to={link} className={styles.link} alt={title}>
              {!!Icon
              && <Icon className={cn(styles.icon, { [styles.active]: link === match.url })} />}
            </Link>
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default Navigate;
