import React from 'react';
import { Switch } from 'react-router-dom';
import {
  Route, Home, Registration, Authorization, Profile, EventCreation,
} from '..';

import BaseLayout from '../../layouts/BaseLayout';

const AppRouter = () => (
  <Switch>
    <Route path="/" exact component={Home} layout={BaseLayout} />
    <Route path="/registration" exact component={Registration} layout={BaseLayout} />
    <Route path="/authorization" exact component={Authorization} layout={BaseLayout} />
    <Route path="/profile" exact component={Profile} layout={BaseLayout} privateRoute />
    <Route path="/chat" exact component={Home} layout={BaseLayout} privateRoute />
    <Route path="/eventCreation" exact component={EventCreation} layout={BaseLayout} privateRoute />
  </Switch>
);

export default AppRouter;
