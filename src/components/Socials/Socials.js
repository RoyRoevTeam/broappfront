import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { setUserAction } from 'reducers/user';

import styles from './Socials.css';
import Button from '../ui-kit/Button/Button';
import { API_URL } from '../../config';
import { socket } from '../..';

const socials = [
  { id: 'facebook', title: 'Facebook' },
  { id: 'vk', title: 'ВКонтакте' }
];

const propTypes = {
  setUser: PropTypes.func.isRequired
};

class Socials extends Component {
  static propTypes = propTypes;

  state = {
    disabled: '',
    provider: null
  };

  componentDidMount() {
    const { setUser } = this.props;

    socket.on('authenticated', user => {
      this.popup.close();
      setUser(user);
    });
  }

  startAuth = provider => () => {
    const { disabled } = this.state;

    if (!disabled) {
      this.setState({ disabled: true, provider }, () => {
        this.popup = this.openPopup();
        this.checkPopup();
      });
    }
  };

  openPopup() {
    const { provider } = this.state;
    const width = 600; const
      height = 600;
    const left = (window.innerWidth / 2) - (width / 2);
    const top = (window.innerHeight / 2) - (height / 2);
    const url = `${API_URL}/auth/${provider}?socketId=${socket.id}`;

    return window.open(url, '',
      `toolbar=no, location=no, directories=no, status=no, menubar=no, 
      scrollbars=no, resizable=no, copyhistory=no, width=${width}, 
      height=${height}, top=${top}, left=${left}`);
  }

  checkPopup() {
    const check = setInterval(() => {
      const { popup } = this;
      if (!popup || popup.closed || popup.closed === undefined) {
        clearInterval(check);
        this.setState({ disabled: false });
      }
    }, 1000);
  }

  render() {
    return (
      <div className={styles.buttonsWrap}>
        {socials.map(({ id, title }) => (
          <span key={id} className={styles.buttonWrap}>
            <Button onClick={this.startAuth(id)} fluid>{title}</Button>
          </span>
        ))}
      </div>
    );
  }
}

const mapDispatchToProps = {
  setUser: setUserAction
};

export default connect(null, mapDispatchToProps)(Socials);
