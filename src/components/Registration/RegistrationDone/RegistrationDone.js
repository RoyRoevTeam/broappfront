import React from 'react';
import PropTypes from 'prop-types';

import * as Content from '../../../layouts/components/Content';
import Button from '../../ui-kit/Button/Button';
import styles from './RegistrationDone.css';

const RegistrationDone = props => {
  return (
    <Content.Head
      title="Регистрация"
      description={'На твой e-mail отправлено письмо.Дождись его (иногда приходит с задержкой/попадает в "Спам").'
      + ' 1й раз пройди по ссылке, чтобы подтвердить электронную почту.'}
      fullHeight
    >
      <div className={styles.btnWrap}>
        <Button fluid>
          Создать событие
        </Button>
      </div>
    </Content.Head>
  );
};

RegistrationDone.propTypes = {

};

export default RegistrationDone;
