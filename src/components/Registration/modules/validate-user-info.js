export default function validateUserInfo(userInfo) {
  const errors = {};
  const { userName, email, password, age, about, pact } = userInfo;
  const isEmailValid = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email);

  if (!userName) {
    errors.userName = 'Это обязательное поле';
  }

  if (!email) {
    errors.email = 'Это обязательное поле';
  } else if (!isEmailValid) {
    errors.email = 'Некорректный адрес';
  }

  if (!password) {
    errors.password = 'Это обязательное поле';
  }

  if (!age) {
    errors.age = 'Это обязательное поле';
  } else if (isNaN(Number(age))) {
    errors.age = 'Возраст - это всего лишь число';
  } else if (Number(age) < 18) {
    errors.age = 'Только 18+';
  }

  if (!about) {
    errors.about = 'Всего пара слов';
  }

  if (!pact) {
    errors.pact = 'Необходимо подтвердить согласие';
  }

  return errors;
}
