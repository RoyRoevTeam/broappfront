import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import UserInfo from './RegistrationUserInfo';
import RegistrationDone from './RegistrationDone';

const Registration = props => {
  const { currentStep } = props;

  switch (currentStep) {
    case 'userInfo':
      return <UserInfo />;
    case 'registrationDone':
      return <RegistrationDone />;
    default:
      return null;
  }
};

Registration.propTypes = {
  currentStep: PropTypes.string
};

Registration.defaultProps = {
  currentStep: 'userInfo'
};

const mapStateToProps = state => ({
  currentStep: state.registration.currentStep
});

export default connect(mapStateToProps)(Registration);
