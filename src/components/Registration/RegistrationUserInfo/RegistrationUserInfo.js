import React from 'react';
import { Link } from 'react-router-dom';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import styles from './RegistrationUserInfo.css';
import { Button, TextInput, Checkbox } from '../../ui-kit';
import { Content } from '../../../layouts';
import { Socials } from '../..';
import { setStep } from '../../../redux/reducers/registration';
import validateUserInfo from '../modules/validate-user-info';

const NEXT_STEP = 'registrationDone';

const RegistrationUserInfo = props => {
  const { handleSubmit, onStepChange } = props;

  return (
    <form onSubmit={handleSubmit(() => onStepChange(NEXT_STEP))}>
      <Content.Head
        title="Регистрация"
        description={'Bro, нам нужны данные о тебе, чтобы другие bro видели кто создал событие, ты можешь дать'
        + ' доступ к одной из своей социальной сети или указать данные самостоятельно.'}
      >
        <Socials />
      </Content.Head>
      <Content.Section>
        <Content.Row>
          <Field component={TextInput} label="Как тебя зовут" name="userName" />
        </Content.Row>
        <Content.Row>
          <Field component={TextInput} label="Дата рождения" name="age" />
        </Content.Row>
        <Content.Row>
          <Field component={TextInput} label="Электронная почта" placeholder="post@mail.com" name="email" />
        </Content.Row>
        <Content.Row>
          <Field component={TextInput} label="Пароль" name="password" password />
        </Content.Row>
        <Content.Row>
          <Field component={Checkbox} label="Я согласен на обработку моих персональных данных" name="pact" />
        </Content.Row>
        <Content.Row>
          <Button type="submit" fluid>
            Продолжить
          </Button>
        </Content.Row>
        <div className={styles.linkRow}>
          <Link to="/">
            У меня уже есть аккаунт?
          </Link>
        </div>
      </Content.Section>
    </form>
  );
};

const mapDispatchToProps = dispatch => ({
  onStepChange: () => dispatch(setStep(NEXT_STEP))
});

RegistrationUserInfo.propTypes = {
  onStepChange: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired
};

const connected = connect(null, mapDispatchToProps)(RegistrationUserInfo);

export default reduxForm({
  form: 'registrationUserInfo',
  validate: validateUserInfo,
  destroyOnUnmount: false
})(connected);
