import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import EventsList from 'components/EventsList';

const Home = () => (
  <Fragment>
    <EventsList />
  </Fragment>
);

Home.propTypes = {
  list: PropTypes.array,
  isLoading: PropTypes.bool
};

Home.defaultProps = {
  list: null,
  isLoading: false
};

export default Home;
