import React, { Component } from 'react';

import EventUserInfo from './EventUserInfo';
import EventCategoryChoose from './EventCategoryChoose';

class EventCreation extends Component {
  state = {
    currentStep: 'eventUserInfo'
  };

  handleCategoryChooseClick = () => {
    this.setState(() => ({
      currentStep: 'eventCategoryChoose'
    }));
  };

  handleCategoryChooseClose = () => {
    this.setState(() => ({
      currentStep: 'eventUserInfo'
    }));
  };

  render() {
    const { currentStep } = this.state;

    switch (currentStep) {
      case 'eventUserInfo':
        return <EventUserInfo onCategoryChoose={this.handleCategoryChooseClick} />;
      case 'eventCategoryChoose':
        return <EventCategoryChoose onCategoryChooseClose={this.handleCategoryChooseClose} />;
      default:
        return null;
    }
  }
}

export default EventCreation;
