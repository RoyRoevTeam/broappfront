import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';

import { createEvent } from 'reducers/events';
import { Content } from '../../layouts';
import {
  TextInput, TextArea, Switch, Button, CustomButton, Icons, Datepicker, TimeInput
} from '../ui-kit';
import styles from './EventUserInfo.css';

export function EventUserInfo({
  onCategoryChoose, selectedCategory, anotherCategory,
  isFullDayEvent, hasAdditionalPersons, handleSubmit, pristine, submitting
}) {
  const categoryButtonValue = selectedCategory === 'другое' ? anotherCategory : selectedCategory;

  return (
    <form onSubmit={handleSubmit}>
      <Content.Head
        title="Создание события"
        description="Расскажи нам какое событие ты хочешь и мы поможем найти тебе единомышленников."
      />
      <Content.Section>
        <Content.Row>
          <CustomButton
            placeholder="Что тебе хочется?"
            value={categoryButtonValue}
            onClick={onCategoryChoose}
            icon={<Icons.ArrowSmall />}
          />
        </Content.Row>
        <Content.Row>
          <Field component={TextInput} placeholder="Где будет событие?" name="address" />
        </Content.Row>
        <Content.Row>
          <Field component={Datepicker} placeholder="Когда?" name="date" />
        </Content.Row>
        <Content.Row>
          <Field component={Switch} fluid label="Весь день" name="fullDay" />
        </Content.Row>
        <Content.Row>
          <div className={styles.timeWrap}>
            <div className={styles.timeCol}>
              <Field component={TimeInput} fluid label="Начало" name="timeFrom" />
            </div>
            {!isFullDayEvent
              && (
              <div className={styles.timeCol}>
                <Field component={TimeInput} fluid label="Конец" name="timeTo" />
              </div>
              )
            }
          </div>
        </Content.Row>
        <Content.Row>
          <Field component={TextInput} placeholder="Название события" name="title" />
        </Content.Row>
        <Content.Row>
          <Field component={TextArea} rows="5" placeholder="Описание" name="description" />
        </Content.Row>
        <Content.Row>
          <Field component={Switch} fluid label="Ограничить количество людей" name="additionalPersons" />
        </Content.Row>
        {hasAdditionalPersons
          && (
          <Content.Row>
            <Field component={TextInput} placeholder="Укажите количество" name="personsCount" />
          </Content.Row>
          )
        }
        <Content.Row>
          <Button
            fluid
            type="submit"
            disabled={pristine || submitting}
          >
            Создать
          </Button>
        </Content.Row>
      </Content.Section>
    </form>
  );
}

EventUserInfo.propTypes = {
  selectedCategory: PropTypes.string,
  anotherCategory: PropTypes.string,
  onCategoryChoose: PropTypes.func.isRequired,
  isFullDayEvent: PropTypes.bool,
  hasAdditionalPersons: PropTypes.bool,
  handleSubmit: PropTypes.func.isRequired,
  pristine: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
};

EventUserInfo.defaultProps = {
  anotherCategory: null,
  isFullDayEvent: false,
  hasAdditionalPersons: false,
  selectedCategory: null
};

const mapStateToProps = state => {
  const eventInfoSelector = formValueSelector('eventCreation');

  return {
    isFullDayEvent: eventInfoSelector(state, 'fullDay'),
    hasAdditionalPersons: eventInfoSelector(state, 'additionalPersons'),
    selectedCategory: eventInfoSelector(state, 'category'),
    anotherCategory: eventInfoSelector(state, 'anotherCategory')
  };
};

const createReduxForm = reduxForm({
  form: 'eventCreation',
  destroyOnUnmount: false
});

const connected = connect(mapStateToProps, {
  onSubmit: createEvent
});

export default connected(createReduxForm(EventUserInfo));
