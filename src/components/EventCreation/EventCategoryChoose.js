import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';

import { Button, TextInput, Radio } from '../ui-kit';
import { Content } from '../../layouts';
import styles from './EventCategoryChoose.css';
import Container from '../../containers/EventCategoriesContainer';

class EventCategoryChoose extends PureComponent {
  render() {
    const {
      onCategoryChooseClose,
      eventCategories,
      selectedCategory
    } = this.props;

    return (
      <Content.Head
        fullHeight
        title="Что тебя интересует?"
        onClose={onCategoryChooseClose}
      >
        <form>
          {eventCategories.map(category => (
            <div className={styles.radioWrap} key={category}>
              <Field
                component={Radio}
                label={category}
                type="radio"
                value={category}
                name='category'
                fluid
              />
            </div>
          ))}
        </form>
        {selectedCategory === 'другое'
          && (
            <Field
              component={TextInput}
              placeholder="Введите свою категорию"
              name="anotherCategory"
            />
          )
        }
        <div className={styles.buttonWrap}>
          <Button onClick={onCategoryChooseClose} fluid>Выбрать</Button>
        </div>
      </Content.Head>
    );
  }
}

EventCategoryChoose.propTypes = {
  onCategoryChooseClose: PropTypes.func.isRequired,
  eventCategories: PropTypes.arrayOf(PropTypes.string).isRequired,
  selectedCategory: PropTypes.string
};

EventCategoryChoose.defaultProps = {
  selectedCategory: null
};

const connected = Container(EventCategoryChoose);

export default reduxForm({
  form: 'eventCreation',
  destroyOnUnmount: false
})(connected);
