import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { Content } from '../../layouts';

const Profile = ({ user }) => {
  if (!user) {
    return (
      <div>
        Необходимо авторизироваться
      </div>
    );
  }

  const { displayName = 'Бро', percentReady = 0 } = user;

  return (
    <form>
      <Content.Head
        title={displayName}
        description={`Профиль заполнен на ${percentReady}%`}
      />
    </form>
  );
};

Profile.propTypes = {
  user: PropTypes.shape({
    displayName: PropTypes.string,
    percentReady: PropTypes.number
  }).isRequired
};

Profile.defaultProps = {
  // displayName: 'Бро'
};

const mapStateToProps = state => ({
  user: state.user.profile
});

export default connect(mapStateToProps)(Profile);
