import Route from './RouteComponent';
import Home from './Home';
import Registration from './Registration';
import Authorization from './Authorization';
import Socials from './Socials';
import Profile from './Profile';
import EventCreation from './EventCreation';
import EventsList from './EventsList';

export {
  Home,
  Route,
  Registration,
  Authorization,
  Socials,
  Profile,
  EventCreation,
  EventsList
};
