import React from 'react';

import styles from './EventPreview.scss';

const parseDate = date => date.split('T')[0];

const EventPreview = ({ title, date, imgUrl }) => (
  <div className={styles.event}>
    <div className={styles.topBlock}>
      <div className={styles.img} style={{ backgroundImage: `url(${imgUrl})` }} />
      <h3>{title}</h3>
      <div className={styles.date}>
        {date && (<p>{parseDate(date)}</p>)}
      </div>
    </div>
    <div className={styles.buttonGroup}>
      <button type="button" className={styles.btn}>Отложить</button>
      <button type="button" className={styles.btn}>Подробнее</button>
    </div>
  </div>
);

export default EventPreview;
