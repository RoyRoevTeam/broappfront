import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';

const mapStateToProps = ({ user: { profile } = {} }) => ({ profile });

const RouteComponent = ({
  layout: Layout, component: Component, privateRoute, profile, location
}) => {
  if (Layout) {
    return (
      <Route
        render={props => (privateRoute && !profile ? (
          <Redirect to={{
            pathname: '/authorization',
            state: { from: location }
          }}
          />
        ) : (
          <Layout {...props}>
            <Component {...props} />
          </Layout>
        ))}
      />
    );
  }

  return <Route component={Component} />;
};

RouteComponent.propTypes = {
  component: PropTypes.func.isRequired,
  layout: PropTypes.func.isRequired,
  privateRoute: PropTypes.bool,
  profile: PropTypes.shape({}),
  location: PropTypes.shape({})
};

RouteComponent.defaultProps = {
  privateRoute: false,
  profile: null,
  location: null
};

export default connect(mapStateToProps)(RouteComponent);
