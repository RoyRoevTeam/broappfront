import React from 'react';
import PropTypes from 'prop-types';

import TryToAuth from './TryToAuth';
import PasswordRecovery from './PasswordRecovery';
import NewPasswordCreation from './NewPasswordCreation';

const Authorization = props => {
  const { currentStep } = props;

  switch (currentStep) {
    case 'tryToAuth':
      return <TryToAuth />;
    case 'passwordRecovery':
      return <PasswordRecovery />;
    case 'newPasswordCreation':
      return <NewPasswordCreation />;
    default:
      return null;
  }
};

Authorization.propTypes = {
  currentStep: PropTypes.string
};

Authorization.defaultProps = {
  currentStep: 'tryToAuth'
};

export default Authorization;
