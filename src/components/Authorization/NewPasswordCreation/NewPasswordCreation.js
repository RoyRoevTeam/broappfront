import React from 'react';
import * as Content from '../../../layouts/components/Content';
import TextInput from '../../ui-kit/TextInput/TextInput';
import Button from '../../ui-kit/Button/Button';

const NewPasswordCreation = () => {
  return (
    <div>
      <Content.Head
        title="Создание нового пароля"
        description="Придумайте новый, супер простой пароль, чтобы больше не забывать его никогда."
      />
      <Content.Section>
        <Content.Row>
          <TextInput label="Новый пароль" />
        </Content.Row>
        <Content.Row>
          <TextInput label="Подтверждение пароля" />
        </Content.Row>
        <Content.Row>
          <Button fluid>
            Создать
          </Button>
        </Content.Row>
      </Content.Section>
    </div>
  );
};

export default NewPasswordCreation;
