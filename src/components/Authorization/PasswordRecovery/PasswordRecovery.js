import React from 'react';

import { Content } from '../../../layouts';
import { TextInput, Button } from '../../ui-kit';

const PasswordRecovery = () => {
  return (
    <div>
      <Content.Head
        title="Восстановление пароля"
        description="Укажите вашу электронную почту и мы вышлим вам ссылку для создания нового пароля."
      />
      <Content.Section>
        <Content.Row>
          <TextInput label="Электронная почта" />
        </Content.Row>
        <Content.Row>
          <Button fluid>
            Восстановить
          </Button>
        </Content.Row>
      </Content.Section>
    </div>
  );
};

export default PasswordRecovery;
