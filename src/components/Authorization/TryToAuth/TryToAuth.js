import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Form, Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';

import { authUser } from 'reducers/user';
import { Content } from 'layouts';

import { Socials } from '../..';
import { TextInput, Button } from '../../ui-kit';
import styles from '../../Registration/RegistrationUserInfo/RegistrationUserInfo.css';

const TryToAuth = ({
  handleSubmit, pristine, submitting, error
}) => (
  <Form onSubmit={handleSubmit}>
    <Content.Head
      title="Авторизация"
      description="С возвращением, Bro! Давно тебя видно не было, ну заходи, посмотри, что у нас нового."
    >
      <Socials />
    </Content.Head>
    <Content.Section>
      <Content.Row>
        <Field component={TextInput} label="Электронная почта" name="email" />
      </Content.Row>
      <Content.Row>
        <Field component={TextInput} label="Пароль" name="password" password />
      </Content.Row>
      {error && <strong>{error}</strong>}
      <Content.Row>
        <Button
          type="submit"
          fluid
          disabled={pristine || submitting}
        >
            Войти
        </Button>
      </Content.Row>
      <div className={styles.linkRow}>
        <Link to="/">
            Bro забыл пароль?
        </Link>
      </div>
    </Content.Section>
  </Form>
);

TryToAuth.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  pristine: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  error: PropTypes.string
};

TryToAuth.defaultProps = {
  error: null
};

const createReduxForm = reduxForm({
  form: 'authUser'
  // validate: validateUserInfo,
  // destroyOnUnmount: false
});

const connected = connect(null, {
  onSubmit: authUser
});

export default connected(createReduxForm(TryToAuth));
