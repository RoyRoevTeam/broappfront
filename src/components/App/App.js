import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { checkAuthUser as checkAuthUserAction } from 'reducers/user';
import { ConnectedRouter } from 'connected-react-router';

import AppRouter from '../AppRouter';

const mapDispatchToProps = {
  checkAuthUser: checkAuthUserAction
};

const propTypes = {
  checkAuthUser: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired
};

class App extends Component {
  static propTypes = propTypes;

  componentDidMount() {
    const { checkAuthUser } = this.props;

    checkAuthUser();
  }

  render() {
    const { history } = this.props;

    return (
      <ConnectedRouter history={history}>
        <AppRouter />
      </ConnectedRouter>
    );
  }
}

export default connect(null, mapDispatchToProps)(App);
