import React from 'react';
// import PropTypes from 'prop-types';

const MyEvents = ({ className }) => (
  <svg className={className} width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M1 4C1 2.34315 2.34315 1 4 1H18C19.6569 1 21 2.34315 21 4V18.676C21 19.5283 20.002 19.9899 19.3525 19.438L12.9426 13.9912C11.8224 13.0394 10.1776 13.0394 9.05739 13.9912L2.64754 19.438C1.99802 19.9899 1 19.5283 1 18.676V4Z" stroke="black" strokeWidth="2"/>
  </svg>
);

MyEvents.propTypes = {
  // TODO add arrow directions
};

export default MyEvents;
