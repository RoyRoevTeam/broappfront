import React from 'react';
// import PropTypes from 'prop-types';

const Events = ({ className }) => (
  <svg className={className} width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
    <rect x="1" y="1" width="20" height="20" rx="3" stroke="black" strokeWidth="2" />
    <rect y="7" width="22" height="2" rx="1" fill="black" />
  </svg>

);

Events.propTypes = {
  // TODO add arrow directions
};

export default Events;
