import React from 'react';

const ArrowSmall = ({ direction }) => (
  <svg width="10" height="16" viewBox="0 0 10 16" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path fillRule="evenodd" clipRule="evenodd" d="M0.585786 15.4142C-0.195262 14.6332 -0.195262 13.3668 0.585786 12.5858L5.17157 8L0.585786 3.41421C-0.195262 2.63317 -0.195262 1.36683 0.585786 0.585787C1.36683 -0.195263 2.63317 -0.195263 3.41421 0.585787L9.41421 6.58579C10.1953 7.36683 10.1953 8.63316 9.41421 9.41421L3.41421 15.4142C2.63317 16.1953 1.36683 16.1953 0.585786 15.4142Z" fill="#1BD788"/>
  </svg>
);

export default ArrowSmall;
