import React from 'react';
// import PropTypes from 'prop-types';

const Search = ({ className }) => (
  <svg className={className} width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M21 11C21 16.5228 16.5228 21 11 21C5.47715 21 1 16.5228 1 11C1 5.47715 5.47715 1 11 1C16.5228 1 21 5.47715 21 11Z" stroke="white" strokeWidth="2" />
    <path fillRule="evenodd" clipRule="evenodd" d="M17.4262 17.4263C17.8167 17.0358 18.4499 17.0358 18.8404 17.4263L23.7071 22.293C24.0976 22.6835 24.0976 23.3167 23.7071 23.7072C23.3166 24.0978 22.6834 24.0978 22.2929 23.7072L17.4262 18.8405C17.0357 18.45 17.0357 17.8168 17.4262 17.4263Z" fill="white" />
  </svg>
);

Search.propTypes = {
  // TODO add arrow directions
};

export default Search;
