import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './Collapsible.css';

class Collapsible extends Component {
  state = {
    isCollapsed: true
  };

  handleButtonClick = () => {
    const { isCollapsed } = this.state;

    this.setState(() => ({
      isCollapsed: !isCollapsed
    }));
  };

  render() {
    const { label, note, children } = this.props;
    const { isCollapsed } = this.state;

    return (
      <div className={styles.wrapper}>
        <button type="button" className={styles.button} onClick={this.handleButtonClick}>
          <span className={styles.buttonLabel}>
            {label}
          </span>
          {note &&
            <span className={styles.buttonNote}>
              {note}
            </span>
          }
        </button>
        {!isCollapsed &&
          <div className={styles.content}>
            {children}
          </div>
        }
      </div>
    );
  }
}

Collapsible.propTypes = {
  label: PropTypes.string,
  note: PropTypes.string,
  children: PropTypes.node.isRequired
};

Collapsible.defaultProps = {
  label: null,
  note: null
};

export default Collapsible;
