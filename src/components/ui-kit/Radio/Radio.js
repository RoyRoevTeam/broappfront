import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './Radio.css';

const Radio = props => {
  const { label, input, fluid, ...restProps } = props;
  const wrapperClasses = classNames(
    styles.wrapper,
    { [styles['wrapper--fluid']]: fluid === true }
  );

  return (
    <label className={wrapperClasses}>
      <span>{label}</span>
      <input className={styles.input} type="radio" {...restProps} {...input} />
      <span className={styles.radio} />
    </label>
  );
};

Radio.propTypes = {

};

export default Radio;
