import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './FileInput.css';
import { Icons } from '..';

const FileInput = props => {
  const {
    label,
    fluid,
    fileType,
    ...restProps
  } = props;

  const labelClasses = classNames(
    styles.label,
    { [styles['label--fluid']]: fluid === true }
  );

  const getIcon = () => {
    switch (fileType) {
      case 'image':
        return <Icons.Photo />;
      default:
        return null;
    }
  };

  const getAcceptAttribute = () => {
    switch (fileType) {
      case 'image':
        return 'image/jpeg, image/png';
      default:
        return null;
    }
  };

  return (
    <div>
      {label &&
        <span className={styles.textLabel}>{label}</span>
      }
      <label className={labelClasses}>
        <input type="file" className={styles.input} accept={getAcceptAttribute()} {...restProps} />
        {getIcon()}
      </label>
    </div>
  );
};

FileInput.propTypes = {
  label: PropTypes.string,
  fluid: PropTypes.bool,
  fileType: PropTypes.oneOf(['image'])
};

FileInput.defaultProps = {
  label: null,
  fluid: true,
  fileType: null
};

export default FileInput;
