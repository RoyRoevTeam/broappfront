import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './CustomButton.css';

const CustomButton = props => {
  const { label, value, icon, placeholder, ...restProps } = props;
  const buttonClasses = classNames(
    styles.button,
    { [styles['button--hasValue']]: !!value }
  );

  return (
    <div>
      <span className={styles.buttonLabel}>{label}</span>
      <button className={buttonClasses} {...restProps}>
        {value &&
          <span className={styles.valueWrap}>{value}</span>
        }
        {placeholder && !value &&
          <span className={styles.placeholderWrap}>{placeholder}</span>
        }
        {icon &&
          <span className={styles.iconWrap}>{icon}</span>
        }
      </button>
    </div>
  );
};

export default CustomButton;
