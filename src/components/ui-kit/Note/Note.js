import React from 'react';
import PropTypes from 'prop-types';

import styles from './Note.css';

const Note = props => {
  const { children } = props;

  return <span className={styles.wrapper}>{children}</span>;
};

Note.propTypes = {
  children: PropTypes.node.isRequired
};

export default Note;
