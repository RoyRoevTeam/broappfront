import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './TextArea.css';

const TextArea = props => {
  const { label, fluid, input, meta, ...restProps } = props;

  const hasValidationError = meta.touched && meta.error;

  const inputClasses = classNames(
    styles.input,
    {
      [styles['input--hasError']]: hasValidationError,
      [styles['input--fluid']]: fluid === true
    }
  );

  return (
    <label className={styles.wrapper}>
      {label &&
        <span className={styles.label}>{label}</span>
      }
      <textarea className={inputClasses} {...restProps} {...input} />
      {hasValidationError &&
        <span className={styles.errorMessage}>{meta.error}</span>
      }
    </label>
  );
};

TextArea.propTypes = {
  label: PropTypes.string,
  fluid: PropTypes.bool,
  input: PropTypes.object,
  meta: PropTypes.object
};

TextArea.defaultProps = {
  label: null,
  fluid: true,
  input: {},
  meta: {}
};

export default TextArea;
