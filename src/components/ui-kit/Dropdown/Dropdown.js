import React from 'react';
import PropTypes from 'prop-types';

const Dropdown = props => {
  const {
    options, meta, input, ...restProps
  } = props;

  return (
    <select {...restProps} {...input}>
      {options.map(option => (
        <option key={option.value} value={option.value}>
          {option.label}
        </option>
      ))}
    </select>
  );
};

Dropdown.propTypes = {
  options: PropTypes.array.isRequired
};

export default Dropdown;
