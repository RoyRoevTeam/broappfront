import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './Datepicker.css';

class Datepicker extends Component {
  state = {
    isInputEmpty: true
  };

  handleChange = e => {
    e.persist();

    if (!e.target.value) {
      this.setState(() => ({
        isInputEmpty: true
      }));
    } else {
      this.setState(() => ({
        isInputEmpty: false
      }));
    }
  };

  render() {
    const { input, placeholder, ...restProps } = this.props;
    const { isInputEmpty } = this.state;

    const inputClasses = classNames(
      styles.input,
      { [styles['input--hasValue']]: input.value }
    );

    return (
      <div className={styles.wrapper}>
        <input className={inputClasses} type="date" onChange={this.handleChange} {...restProps} {...input} />
        <span className={styles.placeholder}>{placeholder}</span>
      </div>
    );
  }
}

Datepicker.propTypes = {

};

export default Datepicker;
