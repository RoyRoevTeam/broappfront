import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './Switch.css';

const Switch = ({label, fluid, theme, input, ...restProps}) => {
  const labelClasses = classNames(
    styles.label,
    { [styles['label--fluid']]: fluid === true }
  );

  const labelTextClasses = classNames(
    styles.labelText,
    { [styles['labelText--white']]: theme === 'white' }
  );

  const switchBodyClasses = classNames(
    styles.switchBody,
    { [styles['switchBody--white']]: theme === 'white' }
  );

  return (
    <label className={labelClasses}>
      <span className={labelTextClasses}>{label}</span>
      <input className={styles.input} type="checkbox" {...restProps} {...input} />
      <span className={switchBodyClasses} />
    </label>
  );
};

Switch.propTypes = {
  
};

export default Switch;
