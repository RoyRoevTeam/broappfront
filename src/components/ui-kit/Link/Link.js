import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Link as LinkComponent } from 'react-router-dom';

import styles from './Link.css';

const Link = props => {
  const {
    children,
    fluid,
    theme,
    ...restProps
  } = props;

  const classes = classNames(
    styles.button,
    {
      [styles['button--fluid']]: fluid === true,
      [styles['button--primary']]: theme === 'primary'
    }
  );

  return (
    <LinkComponent
      className={classes}
      {...restProps}
    >
      {children}
    </LinkComponent>
  );
};

Link.propTypes = {
  children: PropTypes.node.isRequired,
  fluid: PropTypes.bool,
  theme: PropTypes.oneOf(['primary'])
};

Link.defaultProps = {
  fluid: false,
  theme: 'primary'
};

export default Link;
