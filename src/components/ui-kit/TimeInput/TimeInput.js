import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './TimeInput.css';

TimeInput.propTypes = {

};

export default function TimeInput({ label, input, fluid, ...restProps }) {
  const inputClasses = classNames(
    styles.input,
    { [styles['input--fluid']]: fluid === true }
  );

  return (
    <div className={styles.wrapper}>
      <input className={inputClasses} type="time" {...restProps} {...input} />
      <span className={styles.label}>{label}</span>
    </div>
  );
};
