import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './Button.css';

const Button = props => {
  const {
    children,
    fluid,
    theme,
    ...restProps
  } = props;

  const classes = classNames(
    styles.button,
    {
      [styles['button--fluid']]: fluid === true,
      [styles['button--primary']]: theme === 'primary'
    }
  );

  return (
    <button
      className={classes}
      type="button"
      {...restProps}
    >
      {children}
    </button>
  );
};

Button.propTypes = {
  children: PropTypes.node.isRequired,
  fluid: PropTypes.bool,
  theme: PropTypes.oneOf(['primary'])
};

Button.defaultProps = {
  fluid: false,
  theme: 'primary'
};

export default Button;
