import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './Checkbox.css';

const Checkbox = props => {
  const {
    label, input, meta, ...restProps
  } = props;

  const hasValidationError = meta.touched && meta.error;

  console.log(input);

  const boxClasses = classNames(
    styles.box,
    { [styles['box--hasError']]: hasValidationError }
  );

  const textClasses = classNames(
    styles.text,
    { [styles['text--hasError']]: hasValidationError }
  );

  return (
    <label className={styles.label}>
      <input className={styles.input} type="checkbox" checked={input.value} {...restProps} {...input} />
      <span className={boxClasses} />
      <span className={textClasses}>{label}</span>
    </label>
  );
};

Checkbox.propTypes = {
  label: PropTypes.string,
  input: PropTypes.object,
  meta: PropTypes.object
};

Checkbox.defaultProps = {
  label: null,
  input: {},
  meta: {}
};

export default Checkbox;
