import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './TextInput.css';
import { Icons } from '..';

class TextInput extends Component {
  state = {
    isPasswordMode: false,
    isTextVisible: true
  };

  componentDidMount() {
    const { password } = this.props;

    if (password) {
      this.setState(() => ({
        isPasswordMode: true,
        isTextVisible: false
      }));
    }
  }

  handleEyeClick = () => {
    const { isTextVisible } = this.state;

    this.setState(() => ({
      isTextVisible: !isTextVisible
    }));
  };

  render() {
    const {
      label, fluid, input, meta, placeholder, password, ...restProps
    } = this.props;

    const { isPasswordMode, isTextVisible } = this.state;

    const hasValidationError = meta.touched && meta.error;
    const hasValue = Boolean(input.value);

    const inputClasses = classNames(
      styles.input,
      {
        [styles['input--hasError']]: hasValidationError,
        [styles['input--hasValue']]: hasValue,
        [styles['input--fluid']]: fluid === true
      }
    );

    return (
      <label className={styles.wrapper}>
        {label &&
          <span className={styles.label}>{label}</span>
        }
        <span className={styles.inputWrap}>
          <input
            type={isTextVisible ? 'text' : 'password'}
            className={inputClasses}
            {...restProps}
            {...input}
          />
          <span className={styles.iconsWrap}>
            {meta.valid && !isPasswordMode &&
              <Icons.Check />
            }
            {isPasswordMode &&
              <span className={styles.iconEyeWrap} onClick={this.handleEyeClick}>
                <Icons.Eye closed={!isTextVisible} />
              </span>
            }
          </span>
          {placeholder &&
            <span className={styles.placeholder}>{placeholder}</span>
          }
        </span>
        {hasValidationError &&
          <span className={styles.errorMessage}>{meta.error}</span>
        }
      </label>
    );
  }
}

TextInput.propTypes = {
  label: PropTypes.string,
  fluid: PropTypes.bool,
  input: PropTypes.object,
  meta: PropTypes.object,
  password: PropTypes.bool
};

TextInput.defaultProps = {
  label: null,
  fluid: true,
  input: {},
  meta: {},
  password: false
};

export default TextInput;
