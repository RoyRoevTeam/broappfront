const dev = process.env.NODE_ENV !== 'production';
const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');

const htmlWebpackPlugin = new HtmlWebPackPlugin({
  template: './src/index.html',
  filename: './index.html'
});

const plugins = [
  htmlWebpackPlugin,
  new FriendlyErrorsWebpackPlugin()
];


if (!dev) {
  plugins.push(new BundleAnalyzerPlugin({
    analyzerMode: 'static',
    reportFilename: 'webpack-report.html',
    openAnalyzer: false
  }));
}

const CSSModuleLoader = {
  loader: 'css-loader',
  options: {
    importLoaders: 1,
    modules: true,
    sourceMap: true,
    localIdentName: '[local]__[hash:base64:5]',
    minimize: true
  }
};

module.exports = {
  entry: './src/index.js',
  output: {
    publicPath: '/',
    path: path.join(__dirname, 'build'),
    filename: 'main.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.scss$/,
        use: [
          'style-loader', // creates style nodes from JS strings
          CSSModuleLoader, // translates CSS into CommonJS
          'sass-loader', // compiles Sass to CSS, using Node Sass by default
          {
            loader: 'sass-resources-loader',
            options: {
              // Provide path to the file with resources
              resources: './src/styles/resources.scss'
            }
          }
        ]
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          CSSModuleLoader
        ]
      }
    ]
  },
  plugins,
  devServer: {
    historyApiFallback: true
  },
  resolve: {
    alias: {
      reducers: path.resolve(__dirname, './src/redux/reducers'),
      containers: path.resolve(__dirname, './src/containers/'),
      constants: path.resolve(__dirname, './src/constants/'),
      config: path.resolve(__dirname, './src/config/'),
      components: path.resolve(__dirname, './src/components/'),
      uiKit: path.resolve(__dirname, './src/components/ui-kit'),
      layouts: path.resolve(__dirname, './src/layouts/'),
      styles: path.resolve(__dirname, './src/styles/'),
      utils: path.resolve(__dirname, './src/utils/')
    }
  }
};
