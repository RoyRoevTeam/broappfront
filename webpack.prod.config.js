const merge = require('webpack-merge');

module.exports = merge(require('./webpack.config'), {
  mode: 'production',
  devtool: 'source-map',
  optimization: {
    splitChunks: {
      // include all types of chunks
      chunks: 'all'
    }
  },
  performance: { hints: false }
});
