import express from 'express';
import React from 'react';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';
import { Provider as ReduxProvider } from 'react-redux';
import Helmet from 'react-helmet';
import Layout from '../src/components/AppContainer';
import createStore, { initializeSession } from '../src/redux/store';

const app = express();

// app.use(express.static(path.resolve(__dirname, '../dist')));

function htmlTemplate(reactDom, reduxState, helmetData) {
  return `
        <!DOCTYPE html>
        <html>
        <head>
            <meta charset="utf-8">
            ${helmetData.title.toString()}
            ${helmetData.meta.toString()}
            <title>React SSR</title>
        </head>
        
        <body>
            <div id="root">${reactDom}</div>
            <script>
                window.REDUX_DATA = ${JSON.stringify(reduxState)}
            </script>
            <script src="./build/main.js"></script>
            <script src="./build/1.main.js"></script>
        </body>
        </html>
    `;
}

app.get('/*', (req, res) => {
  const context = { };
  const store = createStore();

  store.dispatch(initializeSession());

  const jsx = (
    <ReduxProvider store={store}>
      <StaticRouter context={context} location={req.url}>
        <Layout />
      </StaticRouter>
    </ReduxProvider>
  );
  const reactDom = renderToString(jsx);
  const reduxState = store.getState();
  const helmetData = Helmet.renderStatic();

  res.writeHead(200, { 'Content-Type': 'text/html' });
  res.end(htmlTemplate(reactDom, reduxState, helmetData));
});

app.listen(2048);
