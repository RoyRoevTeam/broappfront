require('babel-register')({
  presets: ['env', 'react'],
  plugins: ['transform-object-rest-spread', 'transform-class-properties', 'react-hot-loader/babel']
});

require('./server');
